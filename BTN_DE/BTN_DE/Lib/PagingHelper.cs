﻿using System.Collections.Generic;

namespace BTN_DE.Lib
{
    public class PagingInformation<T>
    {
        public int Pages { get; set; }
        public int PageItems { get; set; }
        public int CurrentPageNumber { get; set; }
        public string DefaultPageAddress { get; set; }
        public string FirstPageAddress { get; set; }
        public string PreviousPageAddress { get; set; }
        public string CurrentPageAddress { get; set; }
        public string NextPageAddress { get; set; }
        public string LastPageAddress { get; set; }
        public List<T> Items { get; set; }
        public string PaginationHtml { get; set; }

        public PagingInformation(int pageItems, string defaultPageAddress)
        {
            PageItems = pageItems;
            DefaultPageAddress = defaultPageAddress;

            CurrentPageNumber = 0;
            FirstPageAddress = "";
            PreviousPageAddress = "";
            CurrentPageAddress = "";
            NextPageAddress = "";
            LastPageAddress = "";
            Items = new List<T>();
            PaginationHtml = "";
        }
    }

    public class PagingHelper<T>
    {
        public static string PagingAddHtml(PagingInformation<T> source,
            int headPageButtons, int tailPageButtons, int totalPageButtons)
        {
            if (source == null)
            {
                return "";
            }

            source.PaginationHtml += "<ul class=\"pagination\">";

            if (!string.IsNullOrEmpty(source.FirstPageAddress))
            {
                source.PaginationHtml += "<li><a href=\" " + source.FirstPageAddress + "\">&laquo;&laquo;</a></li>";
            }

            if (!string.IsNullOrEmpty(source.PreviousPageAddress))
            {
                source.PaginationHtml += "<li><a href=\" " + source.PreviousPageAddress + "\">&laquo;</a></li>";
            }

            if (headPageButtons == 3)
            {
                source.PaginationHtml += "<li class=\"disabled\"><a href=\" " + source.PreviousPageAddress + "\">...</a></li>";
            }

            int startPageNumber = 1;
            int endPageNumber = source.Pages;
            if (source.Pages + headPageButtons + tailPageButtons > totalPageButtons * 2 + 1)
            {
                startPageNumber = source.CurrentPageNumber - (totalPageButtons - headPageButtons);
                if (startPageNumber < 1)
                {
                    startPageNumber = 1;
                }

                endPageNumber = source.CurrentPageNumber + (totalPageButtons - tailPageButtons);
                if (startPageNumber > source.CurrentPageNumber - (totalPageButtons - headPageButtons))
                {
                    endPageNumber += -(source.CurrentPageNumber - (totalPageButtons - headPageButtons)) + 1;
                }

                if (endPageNumber > source.Pages)
                {
                    endPageNumber = source.Pages;
                }

                if (source.Pages - source.CurrentPageNumber - (totalPageButtons - tailPageButtons) < 0)
                {
                    startPageNumber += (source.Pages - source.CurrentPageNumber - (totalPageButtons - tailPageButtons));
                }
            }

            for (int i = startPageNumber; i <= endPageNumber; i++)
            {
                if (i == source.CurrentPageNumber)
                {
                    source.PaginationHtml += "<li class=\"disabled\"><a href=\"#\">" + i.ToString() + "</a></li>";
                }
                else
                {
                    string page = source.DefaultPageAddress + i.ToString();
                    source.PaginationHtml += "<li><a href=\"" + page + "\"> " + i.ToString() + "</a></li>";
                }
            }

            if (tailPageButtons == 3)
            {
                source.PaginationHtml += "<li class=\"disabled\"><a href=\" " + source.PreviousPageAddress + "\">...</a></li>";
            }

            if (!string.IsNullOrEmpty(source.NextPageAddress))
            {
                source.PaginationHtml += "<li><a href=\" " + source.NextPageAddress + "\">&raquo;</a></li>";
            }

            if (!string.IsNullOrEmpty(source.LastPageAddress))
            {
                source.PaginationHtml += "<li><a href=\" " + source.LastPageAddress + "\">&raquo;&raquo;</a></li>";
            }

            source.PaginationHtml += "</ul>";

            return source.PaginationHtml;
        }

        public static PagingInformation<T> Paging(int pageItems, string defaultPageAddress,
            int currentPageNumber, List<T> items, int totalPageButtons = 7)
        {
            PagingInformation<T> result = new PagingInformation<T>(pageItems, defaultPageAddress)
            {
                CurrentPageNumber = currentPageNumber
            };

            if (result.CurrentPageNumber > 0)
            {
                result.CurrentPageAddress = result.DefaultPageAddress + result.CurrentPageNumber;
                int headPageButtons = 0;
                int tailPageButtons = 0;

                if (result.CurrentPageNumber > 2)
                {
                    result.FirstPageAddress = result.DefaultPageAddress + "1";
                    headPageButtons++;
                }

                if (result.CurrentPageNumber > 1)
                {
                    result.PreviousPageAddress = result.DefaultPageAddress + (result.CurrentPageNumber - 1).ToString();
                    headPageButtons++;
                }

                if (result.CurrentPageNumber - (totalPageButtons - headPageButtons) > 0)
                {
                    headPageButtons++;
                }

                int itemsCount = items.Count;
                result.Pages = itemsCount / result.PageItems;
                if (result.Pages == 0 
                    || itemsCount % result.Pages != 0)
                {
                    result.Pages++;
                }

                int startIndex = (result.CurrentPageNumber - 1)*result.PageItems;            

                if (result.CurrentPageNumber < result.Pages)
                {
                    result.NextPageAddress = result.DefaultPageAddress + (result.CurrentPageNumber + 1).ToString();
                    tailPageButtons++;
                }

                if (result.CurrentPageNumber < result.Pages - 1)
                {
                    result.LastPageAddress = result.DefaultPageAddress + result.Pages.ToString();
                    tailPageButtons++;
                }

                if (result.CurrentPageNumber - (totalPageButtons - tailPageButtons) < result.Pages - totalPageButtons - tailPageButtons)
                {
                    tailPageButtons++;
                }

                if (result.CurrentPageNumber <= result.Pages)
                {
                    result.Items = result.CurrentPageNumber*result.PageItems > itemsCount
                        ? items.GetRange(startIndex, itemsCount - startIndex)
                        : items.GetRange(startIndex, result.PageItems);
                }

                PagingAddHtml(result, headPageButtons, tailPageButtons, totalPageButtons);
            }

            return result;
        }
    }
}