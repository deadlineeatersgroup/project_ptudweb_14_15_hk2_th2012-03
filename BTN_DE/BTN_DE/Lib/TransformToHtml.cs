﻿using BTN_DE.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BTN_DE.Lib
{
    public class TransformToHtml
    {
        public static string BookShowTransform(List<SACH> sachs)
        {
            string bookresult = "";
            foreach (SACH sa in sachs)
            {
                bookresult += "<div class=\"col-sm-3 book-item\">";
                bookresult += "<div class=\"product-image-wrapper\">";
                bookresult += "<div class=\"single-products\">";
                bookresult += "<div class=\"productinfo text-center\">";
                bookresult += "<img class=\"image-showhome\" src=\"" + sa.TrangBia + "\" alt=\"\" />";
                bookresult += "<h3 class=\"book-title\">" + sa.TenSach + "</h3>";
                bookresult += "<p class=\"book-author\">Tác giả: " + sa.TacGia + "</p>";
                bookresult += "</div>";
                bookresult += "<div class=\"product-overlay\">";
                bookresult += "<div class=\"overlay-content\">";
                bookresult += "<h3>" + sa.TenSach + "</h3>";
                bookresult += "<p>Giá bán: " + sa.GiaBan.ToString() + "</p>";
                bookresult += "<a href=\"/Cart/AddToCart?bookID="+sa.MaSach+"\" class=\"btn btn-default add-to-cart\"><i class=\"fa fa-shopping-cart\"></i>Add to cart</a>";
                bookresult += "</div>";
                bookresult += "</div>";
                bookresult += "</div>";
                bookresult += "<div class=\"choose\">";
                bookresult += "<ul class=\"nav nav-pills nav-justified\">";
                bookresult += "<li><a href=\"/Product/DetailProduct/" + sa.MaSach + "\"><i class=\"fa fa-plus-square\"></i>Xem chi tiết</a></li>";
                bookresult += "<li><a href=\"#\"><i class=\"fa fa-plus-square\"></i>So sánh</a></li>";
                bookresult += "</ul>";
                bookresult += "</div>";
                bookresult += "</div>";
                bookresult += "</div>";
            }
            return bookresult;
        }
        public static string BookManagerTransform(List<SACH> sachs)
        {
            string bookresult = "";
            for (int i = 0; i < sachs.Count(); i++)
            {
                bookresult += "<div class=\"col-sm-3\">";
                bookresult += "<div class=\"product-image-wrapper\">";
                bookresult += "<div class=\"single-products\">";
                bookresult += "<div class=\"productinfo text-center\">";
                if (sachs[i].TrangBia != null)
                {
                    bookresult += "<img class=\"face-product\" src=\"" + sachs[i].TrangBia + "\" alt=\"\" />";
                }
                else
                {
                    bookresult += "<img class=\"face-product\" src=\"" + "../../Images/Items/default-book.png" + "\" alt=\"\" />";
                }

                bookresult += "<a href=\"#\" class=\"book-title\" title=\"" + sachs[i].TenSach + "\">" + sachs[i].TenSach + "</a>";
                bookresult += "<a href=\"#\" class=\"book-author\" title=\"" + sachs[i].TacGia + "\">Tác giả:" + sachs[i].TacGia + "</a>";
                bookresult += "</div>";
                bookresult += "</div>";
                bookresult += "<div class=\"choose\">";
                bookresult += "<ul class=\"nav nav-pills nav-justified\">";
                bookresult += "<li><a href=\"/Product/UpdateProduct/" + sachs[i].MaSach + "\"><i class=\"fa  fa-wrench\"></i>Cập nhật</a></li>";
                bookresult += "<li><a href=\"/Product/DeleteProduct/" + sachs[i].MaSach + "\" class=\"btn-delete\"><i class=\"fa fa-trash-o\"></i>Bỏ sách</a></li>";
                bookresult += "</ul>";
                bookresult += "</div>";
                bookresult += "</div>";
                bookresult += "</div>";
            }
            return bookresult;
        }
    }
}