﻿using BTN_DE.Lib;
using BTN_DE.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BTN_DE.Controllers
{
    public class HomeController : Controller
    {
        private QLSachEntities db = new QLSachEntities();
        //
        // GET: /Home/

        public ActionResult Index()
        {
            string newbook = "";
            List<SACH> sachs = db.SACHes.OrderByDescending(s=>s.STT).Take(8).ToList();
            newbook = TransformToHtml.BookShowTransform(sachs);
            ViewBag.NewBook = newbook;

            string favoriteBook = "";
            sachs = db.SACHes.OrderByDescending(s => s.STT).Take(4).ToList();
            favoriteBook = TransformToHtml.BookShowTransform(sachs);
            ViewBag.FavoriteBookItem1 = favoriteBook;

            
            sachs = db.SACHes.OrderByDescending(s => s.STT).Skip(4).Take(4).ToList();
            favoriteBook = favoriteBook = TransformToHtml.BookShowTransform(sachs);
            ViewBag.FavoriteBookItem2 = favoriteBook;

            return View();
        }
    }
}
