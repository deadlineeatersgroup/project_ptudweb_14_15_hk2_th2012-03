﻿using System.Web.Mvc;

namespace BTN_DE.Controllers.Admin
{
    [Authorize(Roles = "Admin, Staff")]
    public class AdminAnalysController : Controller
    {
        //
        // GET: /AdminAnalys/

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Main()
        {
            return View();
        }

    }
}
