﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BTN_DE.Models;
using BTN_DE.Lib;
using System.Data.Entity;
using System.Data.Entity.Validation;

namespace BTN_DE.Controllers.Admin
{
    [Authorize(Roles = "Admin, Staff")]
    public class AdminNewsManagerController : Controller
    {
        //
        // GET: /AdminNewsManager/

        private QLSachEntities db = new QLSachEntities();
        public ActionResult ShowAllNewsManager(int pagenumber = 1)
        {
            List<TINTUC> news = db.TINTUCs.OrderByDescending(s => s.ID).Skip((pagenumber - 1) * 5).Take(5).ToList();

            //
            int sumnews = db.TINTUCs.Count();
            int pagecount = 0;
            if (sumnews % 5 == 0)
            {
                pagecount = sumnews / 5;
            }
            else
            {
                pagecount = sumnews / 5 + 1;
            }


            string htmlpaging = "";
            for (int i = 1; i <= pagecount; i++)
            {
                if (i != pagenumber)
                {
                    htmlpaging += "<li><a href=\"" + Url.Action("ShowAllNewsManager", "AdminNewsManager", new { pagenumber = i }
                        ).ToString() + "\">" + i.ToString() + "</a></li>";
                }
                else
                {
                    htmlpaging += "<li " + "class=\"disabled\"" + "><a href=\"#\">" + i.ToString() + "</a></li>";
                }
            }
            ViewBag.HtmlPaging = htmlpaging;

            return View(news);
        }

        //
        // GET: /AdminProductManager/AddNewsmanager
        public ActionResult AddNewsManager()
        {
            List<TINTUC> tintucs = db.TINTUCs.ToList();

            return View();
        }

        //
        // POST: /AdminNewsManager/AddNewsManager

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AddNewsManager(TINTUC tt, FormCollection from)
        {
            if (ModelState.IsValid)
            {
                //get images
                foreach (string item in Request.Files)
                {
                    HttpPostedFileBase file = Request.Files[item] as HttpPostedFileBase;
                    if (file.ContentLength == 0)
                        continue;

                    if (file.ContentLength > 0)
                    {
                        ImageUpload imageUpload = new ImageUpload { Width = 600 };
                        ImageResult imageResult = imageUpload.RenameUploadFile(file);

                        if (imageResult.Success)
                        {
                            tt.MINHHOA = "/Images/News/" + imageUpload.FileName;
                        }
                        else
                        {

                        }
                    }
                }

                int size = db.TINTUCs.ToList().Count();
                if (size > 0)
                {
                    CONFIG ids = db.CONFIGs.ToList().Last();
                    ids.NewsIndexID++;
                    int? newindexID = ids.NewsIndexID;                 
                    db.Entry(ids).State = EntityState.Modified;
                    db.SaveChanges();
                    string newID = (newindexID).ToString();
                    tt.ID = newID;
                    tt.NGAYTAO = DateTime.Now;
                }
                else
                {
                    tt.ID = "1";
                    tt.NGAYTAO = DateTime.Now;
                }
                db.TINTUCs.Add(tt);

                try
                {
                    db.SaveChanges();
                }
                catch (DbEntityValidationException e)
                {
                    foreach (var eve in e.EntityValidationErrors)
                    {
                        Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                            eve.Entry.Entity.GetType().Name, eve.Entry.State);
                        foreach (var ve in eve.ValidationErrors)
                        {
                            Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
                                ve.PropertyName, ve.ErrorMessage);
                        }
                    }
                    throw e;
                }
                
                return RedirectToAction("ShowAllNewsManager");
            }

            return View();
        }

        //Cập nhật tin tức
        // GET: /AdminNewsManager/UpdateNews
        public ActionResult UpdateNews(string id = null)
        {
            TINTUC tt = db.TINTUCs.Find(id);
            if (tt == null)
            {
                return HttpNotFound();
            }

            return View(tt);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult UpdateNews(TINTUC tt, string id, FormCollection formCollection)
        {
            string messageUpdate = "";
            if (ModelState.IsValid)
            {
                //get images
                foreach (string item in Request.Files)
                {
                    HttpPostedFileBase file = Request.Files[item] as HttpPostedFileBase;
                    if (file.ContentLength == 0)
                        continue;

                    if (file.ContentLength > 0)
                    {
                        ImageUpload imageUpload = new ImageUpload { Width = 600 };
                        ImageResult imageResult = imageUpload.RenameUploadFile(file);

                        if (imageResult.Success)
                        {
                            tt.MINHHOA = "/Images/News/" + imageUpload.FileName;
                        }
                        else
                        {

                        }
                    }
                }
                tt.ID = id;
                db.Entry(tt).State = EntityState.Modified;
                db.SaveChanges();
                messageUpdate = "<h2>Cập nhật thành công</h2>";
            }
            ViewBag.UpdateMessage = messageUpdate;
            return View(tt);
        }

        //Xóa tin tức

        //
        // GET: /Product/DeleteProduct
        public ActionResult DeleteNews(string id = null)
        {
            TINTUC tt = db.TINTUCs.Find(id);
            if (tt == null)
            {
                return HttpNotFound();
            }
            return View(tt);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteNews(string id, bool isallow = true)
        {
            if (isallow == true)
            {
                try
                {
                    TINTUC tt = db.TINTUCs.Find(id);
                    db.TINTUCs.Remove(tt);
                    db.SaveChanges();
                    return RedirectToAction("ShowAllNewsManager");
                }
                catch (Exception)
                {
                    return RedirectToAction("DatabaseError", "Error");
                }
            }
            else
            {
                return RedirectToAction("ShowAllNewsManager");
            }
        }
    }
}
