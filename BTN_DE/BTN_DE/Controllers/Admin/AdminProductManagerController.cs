﻿using BTN_DE.Lib;
using BTN_DE.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BTN_DE.Controllers.Admin
{
    [Authorize(Roles = "Admin, Staff")]
    public class AdminProductManagerController : Controller
    {
        private QLSachEntities db = new QLSachEntities();

        //
        // GET: /AdminProductManager/

        public ActionResult Index()
        {
            return View();
        }
        //
        // GET: /AdminProductManager/ShowAllProductManager
        public ActionResult ShowAllProductManager(int pagenumber = 1)
        {
            List<SACH> sachs = db.SACHes.OrderByDescending(m => m.STT).Skip((pagenumber - 1) * 12).Take(12).ToList();

            int sumproduct = db.SACHes.Count();

            int pagecount = 0;
            if (sumproduct % 12 == 0)
            {
                pagecount = sumproduct / 12;
            }
            else
            {
                pagecount = sumproduct / 12 + 1;
            }
            string htmlpaging = "";
            for (int i = 1; i <= pagecount; i++)
            {
                if (i != pagenumber)
                {
                    htmlpaging += "<li><a href=\"" + Url.Action("ShowAllProductManager", "AdminProductManager", new { pagenumber = i }
                        ).ToString() + "\">" + i.ToString() + "</a></li>";
                }
                else
                {
                    htmlpaging += "<li " + "class=\"disabled\"" + "><a href=\"#\">" + i.ToString() + "</a></li>";
                }
            }
            ViewBag.HtmlPaging = htmlpaging;

            return View(sachs);
        }

        //
        // GET: /AdminProductManager/AddProductmanager
        public ActionResult AddProductManager()
        {
            List<SACH> saches = db.SACHes.ToList();
            ViewBag.NhaXuatBan = new SelectList(db.NHAXUATBANs, "MaNXB", "TenNXB");
            ViewBag.NhomSach = new SelectList(db.NHOMSACHes, "MaNhom", "TenNhom");
            return View();
        }

        //
        // POST: /AdminProductManager/AddProductManager

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AddProductManager(SACH sach, FormCollection formCollection)
        {
            if (ModelState.IsValid)
            {
                //get images
                foreach (string item in Request.Files)
                {
                    HttpPostedFileBase file = Request.Files[item] as HttpPostedFileBase;
                    if (file.ContentLength == 0)
                        continue;

                    if (file.ContentLength > 0)
                    {
                        ImageUpload imageUpload = new ImageUpload { Width = 600 };
                        ImageResult imageResult = imageUpload.RenameUploadFile(file);

                        if (imageResult.Success)
                        {
                            sach.TrangBia = "/Images/Items/" + imageUpload.FileName;
                        }
                        else
                        {

                        }
                    }
                }
                int size = db.SACHes.ToList().Count();
                if (size > 0)
                {
                    CONFIG ids = db.CONFIGs.ToList().Last();
                    int? newindexID = ids.BookIndexID + 1;
                    ids.BookIndexID++;
                    db.Entry(ids).State = EntityState.Modified;
                    db.SaveChanges();
                    string newID = "SA" + (newindexID + 1).ToString();
                    sach.MaSach = newID;
                    sach.STT = newindexID;
                    sach.TinhTrang = "Còn Hàng";
                    sach.LuotMua = 0;
                    sach.LuotXem = 0;
                    sach.DanhGia = 0;
                    sach.LuotDanhGia = 0;
                }
                else
                {
                    sach.MaSach = "SA1";
                }
                db.SACHes.Add(sach);
                db.SaveChanges();
                return RedirectToAction("ShowAllProductManager");
            }
            ViewBag.NhaXuatBan = new SelectList(db.NHAXUATBANs, "MaNXB", "TenNXB");
            ViewBag.NhomSach = new SelectList(db.NHOMSACHes, "MaNhom", "TenNhom");
            return View();
        }

        //
        // GET: /AdminProductManager/UpdateProduct
        public ActionResult UpdateProduct(string id = null)
        {
           SACH sach = db.SACHes.Find(id);
            if (sach == null)
            {
                return HttpNotFound();
            }
            ViewBag.NhaXuatBan = new SelectList(db.NHAXUATBANs, "MaNXB", "TenNXB", sach.NhaXuatBan);
            ViewBag.NhomSach = new SelectList(db.NHOMSACHes, "MaNhom", "TenNhom", sach.NhomSach);
            return View(sach);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult UpdateProduct(SACH sach, string id, FormCollection formCollection)
        {
            string messageUpdate = "";
            if (ModelState.IsValid)
            {
                //get images
                foreach (string item in Request.Files)
                {
                    HttpPostedFileBase file = Request.Files[item] as HttpPostedFileBase;
                    if (file.ContentLength == 0)
                        continue;

                    if (file.ContentLength > 0)
                    {
                        ImageUpload imageUpload = new ImageUpload { Width = 600 };
                        ImageResult imageResult = imageUpload.RenameUploadFile(file);

                        if (imageResult.Success)
                        {
                            sach.TrangBia = "/Images/Items/" + imageUpload.FileName;
                        }
                        else
                        {

                        }
                    }
                }
                sach.MaSach = id;
                sach.STT = int.Parse(id.Substring(2));
                db.Entry(sach).State = EntityState.Modified;
                db.SaveChanges();
                messageUpdate = "<h2>Cập nhật thành công</h2>";
            }
            ViewBag.UpdateMessage = messageUpdate;
            ViewBag.NhaXuatBan = new SelectList(db.NHAXUATBANs, "MaNXB", "TenNXB", sach.NhaXuatBan);
            ViewBag.NhomSach = new SelectList(db.NHOMSACHes, "MaNhom", "TenNhom", sach.NhomSach);
            return View(sach);
        }

        //
        // GET: /Product/DeleteProduct
        public ActionResult DeleteProduct(string id = null)
        {
            SACH sach = db.SACHes.Find(id);
            if (sach == null)
            {
                return HttpNotFound();
            }
            return View(sach);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteProduct(string id, bool isallow = true)
        {
            if (isallow == true)
            {
                try
                {
                    SACH sach = db.SACHes.Find(id);
                    db.SACHes.Remove(sach);
                    db.SaveChanges();
                    return RedirectToAction("ShowAllProductManager");
                }
                catch (Exception)
                {
                    return RedirectToAction("DatabaseError", "Error");
                }
            }
            else
            {
                return RedirectToAction("ShowAllProductManager");
            }
        }
    }
}
