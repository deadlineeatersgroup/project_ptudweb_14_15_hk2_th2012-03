﻿using BTN_DE.Lib;
using BTN_DE.Models;
using BTN_DE.Models.Bundle;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BTN_DE.Controllers
{
    public class ProductSearchPagingController : Controller
    {
        private QLSachEntities db = new QLSachEntities();
        //
        // GET: /ProductSearchPaging/

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult SearchName(string searchtextname, int pagenumber = 1)
        {
            List<SACH> sachs = new List<SACH>();
            if (searchtextname == null)
            {
                searchtextname = "";
                sachs = db.SACHes.Where(m => m.TenSach.Contains(searchtextname)).OrderByDescending(m => m.STT).Skip((pagenumber - 1) * 8).Take(8).ToList();
            }
            else
            {
                sachs = db.SACHes.Where(m => m.TenSach.Contains(searchtextname)).OrderByDescending(m => m.STT).Skip((pagenumber - 1) * 8).Take(8).ToList();
            }
            string resulthtml = "";
            resulthtml = TransformToHtml.BookShowTransform(sachs);
            int sumproduct = db.SACHes.Where(m => m.TenSach.Contains(searchtextname)).Count();
            int pagecount = 0;
            if (sumproduct % 8 == 0)
            {
                pagecount = sumproduct / 8;
            }
            else
            {
                pagecount = sumproduct / 8 + 1;
            }
            string temp = searchtextname;
            string htmlpaging = "";
            for (int i = 1; i <= pagecount; i++)
            {
                if (i != pagenumber)
                {
                    htmlpaging += "<li><a href=\"" + Url.Action("SearchName", "ProductSearchPaging", new { searchtextname = temp, pagenumber = i }).ToString() + "\">" + i.ToString() + "</a></li>";
                }
                else
                {
                    htmlpaging += "<li " + "class=\"disabled\"" + "><a href=\"#\">" + i.ToString() + "</a></li>";
                }
            }
            ViewBag.BookResult = resulthtml;
            ViewBag.SearchName = searchtextname;
            ViewBag.HtmlPaging = htmlpaging;
            return View();
        }

        public ActionResult SearchByType(string groupid, int pagenumber = 1)
        {

            //LOAISACH ls = db.LOAISACHes.Where(l => l.TenLoai.Contains(typename)).ToList().First();
            NHOMSACH ns = db.NHOMSACHes.Find(groupid);
            List<SACH> sachs = db.SACHes.Where(s => s.NhomSach == groupid).OrderByDescending(m => m.STT).Skip((pagenumber - 1) * 8).Take(8).ToList();
            string resulthtml = "";
            string searchtextname = ns.TenNhom;
            resulthtml = TransformToHtml.BookShowTransform(sachs);
            int sumproduct = db.SACHes.Where(s => s.NhomSach == groupid).OrderByDescending(m => m.STT).Count();
            int pagecount = 0;
            if (sumproduct % 8 == 0)
            {
                pagecount = sumproduct / 8;
            }
            else
            {
                pagecount = sumproduct / 8 + 1;
            }
            string temp = groupid;
            string htmlpaging = "";
            for (int i = 1; i <= pagecount; i++)
            {
                if (i != pagenumber)
                {
                    htmlpaging += "<li><a href=\"" + Url.Action("SearchName", "ProductSearchPaging", new { groupid = temp, pagenumber = i }).ToString() + "\">" + i.ToString() + "</a></li>";
                }
                else
                {
                    htmlpaging += "<li " + "class=\"disabled\"" + "><a href=\"#\">" + i.ToString() + "</a></li>";
                }
            }
            ViewBag.HtmlPaging = htmlpaging;
            ViewBag.BookResult = resulthtml;
            ViewBag.SearchName = searchtextname;
            return View();
        }
        //Tìm theo nhà xuất bản
        public ActionResult SearchByProvider(string ProviderID, int pagenumber = 1)
        {
            //LOAISACH ls = db.LOAISACHes.Where(l => l.TenLoai.Contains(typename)).ToList().First();
            NHAXUATBAN ns = db.NHAXUATBANs.Find(ProviderID);
            List<SACH> sachs = db.SACHes.Where(s => s.NHAXUATBAN1.MaNXB == ProviderID).OrderByDescending(m => m.STT).Skip((pagenumber - 1) * 8).Take(8).ToList();
            string resulthtml = "";
            string searchtextname = ns.TenNXB;
            resulthtml = TransformToHtml.BookShowTransform(sachs);
            int sumproduct = db.SACHes.Where(s => s.NHAXUATBAN1.MaNXB == ProviderID).OrderByDescending(m => m.STT).Count();
            int pagecount = 0;
            if (sumproduct % 8 == 0)
            {
                pagecount = sumproduct / 8;
            }
            else
            {
                pagecount = sumproduct / 8 + 1;
            }
            string temp = ProviderID;
            string htmlpaging = "";
            for (int i = 1; i <= pagecount; i++)
            {
                if (i != pagenumber)
                {
                    htmlpaging += "<li><a href=\"" + Url.Action("SearchByProvider", "ProductSearchPaging", new { ProviderID = temp, pagenumber = i }).ToString() + "\">" + i.ToString() + "</a></li>";
                }
                else
                {
                    htmlpaging += "<li " + "class=\"disabled\"" + "><a href=\"#\">" + i.ToString() + "</a></li>";
                }
            }
            ViewBag.HtmlPaging = htmlpaging;
            ViewBag.BookResult = resulthtml;
            ViewBag.SearchName = searchtextname;
            return View();
        }

        //Search kết hợp
        public ActionResult SearchAdvance(string searchname = "", string searchprovidername = "Nhà Xuất Bản", string searchgroupname = "Nhóm Sách", int searchmincost = 0, int pagenumber = 1)
        {
            BookSearch bs = new BookSearch();
            bs.Name = searchname;
            bs.Provide = searchprovidername;
            bs.Group = searchgroupname;
            bs.Cost = searchmincost;
            string tempprovider = searchprovidername;
            string tempgroup = searchgroupname;
            if (searchprovidername == "Nhà Xuất Bản")
            {
                tempprovider = "";
            }
            if (searchgroupname == "Nhóm Sách")
            {
                tempgroup = "";
            }

            List<SACH> sachs = db.SACHes.Where(m => m.TenSach.Contains(searchname)).Where(m => m.GiaBan > searchmincost).Where(m => m.NhaXuatBan.Contains(tempprovider)).Where(m => m.NhomSach.Contains(tempgroup)).OrderByDescending(m => m.STT).Skip((pagenumber - 1) * 8).Take(8).ToList();
            string resulthtml = "";
            resulthtml = TransformToHtml.BookShowTransform(sachs);
            int sumproduct = db.SACHes.Where(m => m.TenSach.Contains(searchname)).Where(m => m.GiaBan > searchmincost).Where(m => m.NHAXUATBAN1.TenNXB.Contains(tempprovider)).Where(m => m.NHOMSACH1.TenNhom.Contains(tempgroup)).Count();
            int pagecount = 0;
            if (sumproduct % 8 == 0)
            {
                pagecount = sumproduct / 8;
            }
            else
            {
                pagecount = sumproduct / 8 + 1;
            }
            string htmlpaging = "";
            for (int i = 1; i <= pagecount; i++)
            {
                if (i != pagenumber)
                {
                    htmlpaging += "<li><a href=\"" + Url.Action("SearchAdvance", "ProductSearchPaging", new { searchname = bs.Name, searchmincost = bs.Cost, pagenumber = i }
                        ).ToString() + "\">" + i.ToString() + "</a></li>";
                }
                else
                {
                    htmlpaging += "<li " + "class=\"disabled\"" + "><a href=\"#\">" + i.ToString() + "</a></li>";
                }
            }
            ViewBag.HtmlPaging = htmlpaging;
            ViewBag.BookResult = resulthtml;

            ViewBag.NhaXuatBan = new SelectList(db.NHAXUATBANs, "MaNXB", "TenNXB");
            ViewBag.NhomSach = new SelectList(db.NHOMSACHes, "MaNhom", "TenNhom");
            return View(bs);
        }
    }
}
