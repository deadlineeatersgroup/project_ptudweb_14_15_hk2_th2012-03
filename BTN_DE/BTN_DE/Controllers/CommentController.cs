﻿using BTN_DE.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BTN_DE.Controllers
{
    public class CommentController : Controller
    {
        private QLSachEntities db = new QLSachEntities();
        //
        // GET: /Comment/

        public ActionResult Index()
        {
            return View();
        }
        public PartialViewResult AddComment(string Name, string Content, string BookID, int pagenumber = 1)
        {
           DateTime dt = DateTime.Now;
           BookID = BookID.Replace(" ", "");
            string sqlFormattedDateTime = dt.ToString("yyyy-MM-dd HH:mm:ss");
            string sqlFormattedDate =  DateTime.Now.ToString("yyyy-MM-dd");
            string sqlFormattedTime = dt.ToString("HH:mm:ss");
            string idBL = BookID+dt.ToString("yyyyMMddHHmmss");     
            BINHLUAN bl = new BINHLUAN();
            bl.MaBL = idBL;
            bl.Ngay = System.DateTime.Parse(sqlFormattedDate);
            bl.Gio = System.TimeSpan.Parse(sqlFormattedTime);
            bl.NgayGio = System.DateTime.Parse(sqlFormattedDateTime);
            bl.TenNguoiGoi = Name;
            bl.NoiDung = Content;
            bl.MaSach = BookID;
            db.BINHLUANs.Add(bl);
            try
            {
                db.SaveChanges();
            }
            catch(Exception e) { }
            List<BINHLUAN> bls = db.BINHLUANs.Where(b=>b.MaSach == BookID).OrderByDescending(s => s.Ngay).Skip((pagenumber - 1) * 8).Take(8).ToList();
            
            return PartialView("_AddComment", bls);
        }
    }
}
