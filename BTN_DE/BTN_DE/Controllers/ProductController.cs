﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BTN_DE.Models;
using BTN_DE.Lib;

namespace BTN_DE.Controllers
{
    public class ProductController : Controller
    {
        private QLSachEntities db = new QLSachEntities();

        //
        // GET: /Product/

        public ActionResult ShowAllProducts(int pagenumber = 1)
        {
            string bookresult = "";
            List<SACH> sachs = db.SACHes.OrderByDescending(s => s.STT).Skip((pagenumber - 1) * 8).Take(8).ToList();
            bookresult = TransformToHtml.BookShowTransform(sachs);
            int sumproduct = db.SACHes.OrderByDescending(s => s.STT).Count();
            int pagecount = 0;
            if (sumproduct % 8 == 0)
            {
                pagecount = sumproduct / 8;
            }
            else
            {
                pagecount = sumproduct / 8 + 1;
            }
            ViewBag.numpage = pagecount;
            ViewBag.curpage = pagenumber;
            ViewBag.BookResult = bookresult;
            return View();
        }

        //
        // GET: /Product/CheckOut

        public ActionResult CheckOut()
        {
            return View();
        }               

        //
        // GET: /Product/DetailProduct
        public ActionResult DetailProduct(string id = null)
        {
            SACH sach = db.SACHes.Find(id);
            if (sach == null)
            {
                return HttpNotFound();
            }
            sach.LuotXem++;
            db.Entry(sach).State = EntityState.Modified;
            db.SaveChanges();            

            List<BINHLUAN> bls = db.BINHLUANs.Where(b => b.MaSach == id).OrderByDescending(s => s.NgayGio).Take(8).ToList();

            int sumcmt = db.BINHLUANs.Where(b => b.MaSach == id).Count();
            int pagecount = 0;
            if (sumcmt % 8 == 0)
            {
                pagecount = sumcmt / 8;
            }
            else
            {
                pagecount = sumcmt / 8 + 1;
            }

            ViewBag.PageCmtCount = pagecount;
            ViewBag.ListComment = bls;

            return View(sach);
        }

        public PartialViewResult ShowPageProduct(int pagenumber=1)
        {
            List<SACH> sachs = db.SACHes.OrderByDescending(s => s.STT).Skip((pagenumber - 1) * 8).Take(8).ToList();
            int sumproduct = db.SACHes.OrderByDescending(s => s.STT).Count();
            int pagecount = 0;
            if (sumproduct % 8 == 0)
            {
                pagecount = sumproduct / 8;
            }
            else
            {
                pagecount = sumproduct / 8 + 1;
            }
            ViewBag.numpage = pagecount;
            ViewBag.curpage = pagenumber;
            return PartialView("_AddPageProduct",sachs);            
        }
        public PartialViewResult AddComment(string Name, string Content, string BookID, int pagenumber = 1)
        {
            DateTime dt = DateTime.Now;
            BookID = BookID.Replace(" ", "");
            string sqlFormattedDateTime = dt.ToString("yyyy-MM-dd HH:mm:ss");
            string sqlFormattedDate = DateTime.Now.ToString("yyyy-MM-dd");
            string sqlFormattedTime = dt.ToString("HH:mm:ss");
            string idBL = BookID + dt.ToString("yyyyMMddHHmmss");
            BINHLUAN bl = new BINHLUAN();
            bl.MaBL = idBL;
            bl.Ngay = System.DateTime.Parse(sqlFormattedDate);
            bl.Gio = System.TimeSpan.Parse(sqlFormattedTime);
            bl.NgayGio = System.DateTime.Parse(sqlFormattedDateTime);
            bl.TenNguoiGoi = Name;
            bl.NoiDung = Content;
            bl.MaSach = BookID;
            db.BINHLUANs.Add(bl);
            try
            {
                db.SaveChanges();
            }
            catch (Exception e) { }
            List<BINHLUAN> bls = db.BINHLUANs.Where(b => b.MaSach == BookID).OrderByDescending(s => s.NgayGio).Skip((pagenumber - 1) * 8).Take(8).ToList();

            int sumcmt = db.BINHLUANs.Where(b => b.MaSach == BookID).Count();
            int pagecount = 0;
            if (sumcmt % 8 == 0)
            {
                pagecount = sumcmt / 8;
            }
            else
            {
                pagecount = sumcmt / 8 + 1;
            }

            ViewBag.PageCmtCount = pagecount;
            ViewBag.CurPage = pagenumber;
            ViewBag.BookID = BookID;            
            return PartialView("_AddComment", bls);
        }

        public PartialViewResult GetComment(string BookID, int pagenumber = 1)
        {
            List<BINHLUAN> bls = db.BINHLUANs.Where(b => b.MaSach == BookID).OrderByDescending(s => s.NgayGio).Skip((pagenumber - 1) * 8).Take(8).ToList();

            int sumcmt = db.BINHLUANs.Where(b => b.MaSach == BookID).Count();
            int pagecount = 0;
            if (sumcmt % 8 == 0)
            {
                pagecount = sumcmt / 8;
            }
            else
            {
                pagecount = sumcmt / 8 + 1;
            }

            ViewBag.PageCmtCount = pagecount;
            ViewBag.CurPage = pagenumber;
            ViewBag.BookID = BookID;
            return PartialView("_ShowComment", bls);
        }
    }
}

