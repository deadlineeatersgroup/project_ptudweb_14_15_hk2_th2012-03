﻿using BTN_DE.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BTN_DE.Controllers
{
    public class CartController : Controller
    {
        private QLSachEntities db = new QLSachEntities();
        //
        // GET: /Cart/Details/5

        public ActionResult Details()
        {
            return View();
        }

        //
        // GET: /Cart/AddToCart/1

        public ActionResult AddToCart(string bookID, int count = 1)
        {
            if (Session["Cart"] == null)
            {
                List<CartItem> cs = new List<CartItem>();
                CartItem cart = new CartItem();
                SACH sach = db.SACHes.Find(bookID);
                if (sach == null)
                {
                    return HttpNotFound();
                }
                cart.MaSach = sach.MaSach.Replace(" ","");
                cart.TacGia = sach.TacGia;
                if (sach.TenSach.Length > 40)
                {
                    cart.TenSach = sach.TenSach.Substring(0, 39) + "...";
                }
                else
                {
                    cart.TenSach = sach.TenSach;
                }
                cart.NhaXuatBan = sach.NhaXuatBan;
                cart.HinhAnh = sach.TrangBia;
                cart.DonGia = (int)sach.GiaBan;
                cart.SoLuong = count;
                cs.Add(cart);
                Session["Cart"] = cs;
                return RedirectToAction("DetailCart", cs);
            }
            else
            {
                List<CartItem> cs = (List<CartItem>)Session["Cart"];
                for (int i = 0; i < cs.Count(); i++)
                {
                    if (cs[i].MaSach.Equals(bookID))
                    {
                        return RedirectToAction("DetailCart", cs);
                    }
                }
                CartItem cart = new CartItem();
                SACH sach = db.SACHes.Find(bookID);
                if (sach == null)
                {
                    return HttpNotFound();
                }
                cart.MaSach = sach.MaSach.Replace(" ", "");
                cart.TacGia = sach.TacGia;
                if (sach.TenSach.Length > 40)
                {
                    cart.TenSach = sach.TenSach.Substring(0, 39) + "...";
                }
                else
                {
                    cart.TenSach = sach.TenSach;
                }
                cart.NhaXuatBan = sach.NhaXuatBan;
                cart.HinhAnh = sach.TrangBia;
                cart.DonGia = (int)sach.GiaBan;
                cart.SoLuong = count;
                cs.Add(cart);
                Session["Cart"] = cs;
                return RedirectToAction("DetailCart", cs);
            }

        }
        //
        // GET: /Cart/DetailCart
        public ActionResult DetailCart()
        {
            List<CartItem> cs = new List<CartItem>();
            if (Session["Cart"] == null)
            {
                Session["Cart"] = cs;
            }
            else
            {
                cs = (List<CartItem>)Session["Cart"];
                Session["Cart"] = cs;
            }
            return View(cs);
        }

        //
        // GET: /Cart/IncreseItemCount
        public PartialViewResult IncreseItemCount(string bookID)
        {
            List<CartItem> cs = new List<CartItem>();
            if (Session["Cart"] == null)
            {
                Session["Cart"] = cs;
            }
            else
            {
                cs = (List<CartItem>)Session["Cart"];

                for (int i = 0; i < cs.Count; i++)
                {
                    if (cs[i].MaSach.Equals(bookID))
                    {
                        cs[i].SoLuong++;
                        Session["Cart"] = cs;
                    }
                }                
            }
            return PartialView("_UpdateCart",cs);
        }
        //
        // GET: /Cart/IncreseItemCount
        public PartialViewResult DecreseItemCount(string bookID)
        {
            List<CartItem> cs = new List<CartItem>();
            if (Session["Cart"] == null)
            {
                Session["Cart"] = cs;

            }
            else
            {
                cs = (List<CartItem>)Session["Cart"];

                for (int i = 0; i < cs.Count; i++)
                {
                    if (cs[i].MaSach.Equals(bookID))
                    {
                        if (cs[i].SoLuong > 1)
                        {
                            cs[i].SoLuong--;
                        }
                        Session["Cart"] = cs;
                    }
                }
            }
            return PartialView("_UpdateCart", cs);
        }

        //
        // GET: /Cart/IncreseItemCount
        public PartialViewResult DeleteItem(string bookID)
        {
            List<CartItem> cs = new List<CartItem>();
            if (Session["Cart"] == null)
            {
                Session["Cart"] = cs;
            }
            else
            {
                cs = (List<CartItem>)Session["Cart"];

                for (int i = 0; i < cs.Count; i++)
                {
                    if (cs[i].MaSach.Equals(bookID))
                    {
                        cs.RemoveAt(i);
                        Session["Cart"] = cs;
                    }
                }
            }
            return PartialView("_UpdateCart", cs);
        }

    }
}
