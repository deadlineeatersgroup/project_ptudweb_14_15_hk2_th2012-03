﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BTN_DE.Models;

namespace BTN_DE.Controllers
{
    public class NewsController : Controller
    {
        //
        // GET: /News/

        private QLSachEntities db = new QLSachEntities();
        public ActionResult Index(int pagenumber = 1)
        {
            List<TINTUC> news = db.TINTUCs.OrderByDescending(s => s.ID).Skip((pagenumber - 1) * 4).Take(4).ToList();

            //
            int sumnews = db.TINTUCs.Count();
            int pagecount = 0;
            if (sumnews % 4 == 0)
            {
                pagecount = sumnews / 4;
            }
            else
            {
                pagecount = sumnews / 4 + 1;
            }


            string htmlpaging = "";
            for (int i = 1; i <= pagecount; i++)
            {
                if (i != pagenumber)
                {
                    htmlpaging += "<li><a href=\"" + Url.Action("Index", "News", new { pagenumber = i }).ToString() + "\">" + i.ToString() + "</a></li>";
                }
                else
                {
                    htmlpaging += "<li " + "class=\"disabled\"" + "><a href=\"#\">" + i.ToString() + "</a></li>";
                }
            }
            ViewBag.HtmlPaging = htmlpaging;

            //ViewBag.numpage = pagecount;
            // ViewBag.curpage = pagenumber;

            return View(news);
        }

        public PartialViewResult ShowPageNews(int pagenumber = 1)
        {
            List<TINTUC> news = db.TINTUCs.OrderByDescending(s => s.ID).Skip((pagenumber - 1) * 4).Take(4).ToList();

            //
            int sumnews = db.TINTUCs.Count();
            int pagecount = 0;
            if (sumnews % 4 == 0)
            {
                pagecount = sumnews / 4;
            }
            else
            {
                pagecount = sumnews / 4 + 1;
            }

            ViewBag.numpage = pagecount;
            ViewBag.curpage = pagenumber;
            return PartialView("_AddPageNews", news);
        }

        public ActionResult DetailsNews(int id)
        {
            string _id = id.ToString();
            return View(db.TINTUCs.FirstOrDefault(m => m.ID == _id));
        }
    }
}
