﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace BTN_DE
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );
            routes.MapRoute(
                name: "Product",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Product", action = "Products", id = UrlParameter.Optional }
            ); 
            routes.MapRoute(
                 name: "Account",
                 url: "{controller}/{action}/{id}",
                 defaults: new { controller = "Product", action = "ShowAllProducts", id = UrlParameter.Optional }
            );
            routes.MapRoute(
                 name: "Cart",
                 url: "{controller}/{action}/{id}",
                 defaults: new { controller = "Cart", action = "Details", id = UrlParameter.Optional }
            );

            //Book route            
            routes.MapRoute(
                 name: "Error",
                 url: "{controller}/{action}/{id}",
                 defaults: new { controller = "Error", action = "Index", id = UrlParameter.Optional }
            );
            routes.MapRoute(
                 name: "ProductSearchPaging",
                 url: "{controller}/{action}/{id}",
                 defaults: new { controller = "ProductSearchPaging", action = "Index", id = UrlParameter.Optional }
            );

            routes.MapRoute(
                 name: "AdminProductManager",
                 url: "{controller}/{action}/{id}",
                 defaults: new { controller = "AdminProductManager", action = "ShowAllProductManager", id = UrlParameter.Optional }
            );
        }
    }
}