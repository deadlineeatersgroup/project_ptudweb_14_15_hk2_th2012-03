﻿namespace BTN_DE.Models
{
    public class CartItem
    {
        public string MaSach { get; set; }
        public string TenSach { get; set; }
        public string TacGia { get; set; }
        public string NhaXuatBan { get; set; }
        public string HinhAnh { get; set; }
        public int DonGia { get; set; }
        public int SoLuong { get; set; }
    }
}