﻿using Microsoft.AspNet.Identity;
using Microsoft.Owin.Security;
using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Reflection;
using System.Web.Mvc;

namespace BTN_DE.Models
{
    public class IndexViewModel
    {
        public bool HasPassword { get; set; }
        public IList<UserLoginInfo> Logins { get; set; }
        public string PhoneNumber { get; set; }
        public bool TwoFactor { get; set; }
        public bool BrowserRemembered { get; set; }
    }

    public class EditPersonalInfoViewModel
    {
        //[Required(ErrorMessage = "{0} chưa được nhập.")]
        [MaxLength(100, ErrorMessage = "'Tên của bạn' phải có tối đa 100 ký tự.")]
        [Display(Name = "Tên của bạn")]
        public string Name { get; set; }

        [Display(Name = "Giới tính")]
        public int Gender { get; set; }
        public IEnumerable<SelectListItem> GenderList { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd/mm/yyyy}", ApplyFormatInEditMode = true)]
        [Display(Name = "Ngày tháng năm sinh")]
        public DateTime? Birthday { get; set; }

        [MaxLength(200, ErrorMessage = "'Nơi ở hiện tại' phải có tối đa 200 ký tự.")]
        [Display(Name = "Nơi ở hiện tại")]
        public string Address { get; set; }

        [MinLength(3, ErrorMessage = "'Số điện thoại' phải có ít nhất 3 ký tự.")]
        [MaxLength(20, ErrorMessage = "'Số điện thoại' có 'số chữ số' vượt quá mức cho phép")]
        [DataType(DataType.PhoneNumber)]
        [Display(Name = "Số điện thoại")]
        public string PhoneNumber { get; set; }

        public EditPersonalInfoViewModel()
        {
            GenderList = new List<SelectListItem>();
            IEnumerable<Gender> genderType = Enum.GetValues(typeof(Gender)).Cast<Gender>();
            GenderList = from gender in genderType
                         select new SelectListItem
                         {
                             Text = GetDescription(gender),
                             Value = ((int)gender).ToString()
                         };
        }

        public string GetDescription(Enum value)
        {
            Type type = value.GetType();
            string name = Enum.GetName(type, value);

            if (name != null)
            {
                FieldInfo field = type.GetField(name);
                if (field != null)
                {
                    DescriptionAttribute attr = Attribute.GetCustomAttribute(field,
                        typeof(DescriptionAttribute)) as DescriptionAttribute;
                    if (attr != null)
                    {
                        return attr.Description;
                    }
                }
            }

            return null;
        }
    }

    public class ManageLoginsViewModel
    {
        public IList<UserLoginInfo> CurrentLogins { get; set; }
        public IList<AuthenticationDescription> OtherLogins { get; set; }
    }

    public class FactorViewModel
    {
        public string Purpose { get; set; }
    }

    public class SetPasswordViewModel
    {
        [Required(ErrorMessage = "'Mật khẩu mới' chưa được nhập.")]
        [MinLength(6, ErrorMessage = "'Mật khẩu mới' phải có ít nhất 6 ký tự.")]
        [MaxLength(50, ErrorMessage = "'Mật khẩu mới' phải có tối đa 50 ký tự.")]
        [DataType(DataType.Password)]
        [Display(Name = "Mật khẩu mới")]
        public string NewPassword { get; set; }

        [Required(ErrorMessage = "'Nhập lại mật khẩu mới' chưa được nhập.")]
        [DataType(DataType.Password)]
        [Display(Name = "Nhập lại mật khẩu mới")]
        [System.ComponentModel.DataAnnotations.Compare("NewPassword",
            ErrorMessage = "'Nhập lại mật khẩu mới' không khớp với 'Mật khẩu mới'.")]
        public string ConfirmPassword { get; set; }
    }

    public class ChangePasswordViewModel
    {
        [Required(ErrorMessage = "'Mật khẩu hiện tại' chưa được nhập.")]
        [DataType(DataType.Password)]
        [Display(Name = "Mật khẩu hiện tại")]
        public string OldPassword { get; set; }

        [Required(ErrorMessage = "'Mật khẩu mới' chưa được nhập.")]
        [MinLength(6, ErrorMessage = "'Mật khẩu mới' phải có ít nhất 6 ký tự.")]
        [MaxLength(50, ErrorMessage = "'Mật khẩu mới' phải có tối đa 50 ký tự.")]
        [DataType(DataType.Password)]
        [Display(Name = "Mật khẩu mới")]
        public string NewPassword { get; set; }

        [Required(ErrorMessage = "'Nhập lại mật khẩu mới' chưa được nhập.")]
        [DataType(DataType.Password)]
        [Display(Name = "Nhập lại mật khẩu mới")]
        [System.ComponentModel.DataAnnotations.Compare("NewPassword",
            ErrorMessage = "'Nhập lại mật khẩu mới' không khớp với 'Mật khẩu mới'.")]
        public string ConfirmPassword { get; set; }
    }

    public class AddPhoneNumberViewModel
    {
        [Required]
        [Phone]
        [Display(Name = "Phone Number")]
        public string Number { get; set; }
    }

    public class VerifyPhoneNumberViewModel
    {
        [Required]
        [Display(Name = "Code")]
        public string Code { get; set; }

        [Required]
        [Phone]
        [Display(Name = "Phone Number")]
        public string PhoneNumber { get; set; }
    }

    public class ConfigureTwoFactorViewModel
    {
        public string SelectedProvider { get; set; }
        public ICollection<System.Web.Mvc.SelectListItem> Providers { get; set; }
    }

}