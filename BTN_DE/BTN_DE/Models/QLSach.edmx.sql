
-- --------------------------------------------------
-- Entity Designer DDL Script for SQL Server 2005, 2008, 2012 and Azure
-- --------------------------------------------------
-- Date Created: 05/06/2015 22:04:52
-- Generated from EDMX file: D:\temp\BTN_DE\BTN_DE\Models\QLSach.edmx
-- --------------------------------------------------

SET QUOTED_IDENTIFIER OFF;
GO
USE [QLSach];
GO
IF SCHEMA_ID(N'dbo') IS NULL EXECUTE(N'CREATE SCHEMA [dbo]');
GO

-- --------------------------------------------------
-- Dropping existing FOREIGN KEY constraints
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[FK_NHOMSACH_LOAISACH]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[NHOMSACH] DROP CONSTRAINT [FK_NHOMSACH_LOAISACH];
GO
IF OBJECT_ID(N'[dbo].[FK_SACH_NHAXUATBAN]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[SACH] DROP CONSTRAINT [FK_SACH_NHAXUATBAN];
GO
IF OBJECT_ID(N'[dbo].[FK_SACH_NHOMSACH]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[SACH] DROP CONSTRAINT [FK_SACH_NHOMSACH];
GO

-- --------------------------------------------------
-- Dropping existing tables
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[CONFIGID]', 'U') IS NOT NULL
    DROP TABLE [dbo].[CONFIGID];
GO
IF OBJECT_ID(N'[dbo].[LOAISACH]', 'U') IS NOT NULL
    DROP TABLE [dbo].[LOAISACH];
GO
IF OBJECT_ID(N'[dbo].[NHAXUATBAN]', 'U') IS NOT NULL
    DROP TABLE [dbo].[NHAXUATBAN];
GO
IF OBJECT_ID(N'[dbo].[NHOMSACH]', 'U') IS NOT NULL
    DROP TABLE [dbo].[NHOMSACH];
GO
IF OBJECT_ID(N'[dbo].[SACH]', 'U') IS NOT NULL
    DROP TABLE [dbo].[SACH];
GO
IF OBJECT_ID(N'[dbo].[sysdiagrams]', 'U') IS NOT NULL
    DROP TABLE [dbo].[sysdiagrams];
GO
IF OBJECT_ID(N'[QLSachModelStoreContainer].[SACH_FULL]', 'U') IS NOT NULL
    DROP TABLE [QLSachModelStoreContainer].[SACH_FULL];
GO

-- --------------------------------------------------
-- Creating all tables
-- --------------------------------------------------

-- Creating table 'LOAISACHes'
CREATE TABLE [dbo].[LOAISACHes] (
    [MaLoai] char(10)  NOT NULL,
    [TenLoai] nvarchar(50)  NULL
);
GO

-- Creating table 'NHAXUATBANs'
CREATE TABLE [dbo].[NHAXUATBANs] (
    [MaNXB] char(10)  NOT NULL,
    [TenNXB] nvarchar(50)  NULL,
    [DiaChi] nvarchar(50)  NULL
);
GO

-- Creating table 'NHOMSACHes'
CREATE TABLE [dbo].[NHOMSACHes] (
    [MaNhom] char(10)  NOT NULL,
    [TenNhom] nvarchar(50)  NULL,
    [Loai] char(10)  NULL
);
GO

-- Creating table 'SACHes'
CREATE TABLE [dbo].[SACHes] (
    [MaSach] char(10)  NOT NULL,
    [TenSach] nchar(60)  NULL,
    [TacGia] nchar(50)  NULL,
    [SoTrang] int  NULL,
    [GiaBan] int  NULL,
    [NhomSach] char(10)  NULL,
    [TrangBia] nchar(60)  NULL,
    [NhaXuatBan] char(10)  NULL
);
GO

-- Creating table 'sysdiagrams'
CREATE TABLE [dbo].[sysdiagrams] (
    [name] nvarchar(128)  NOT NULL,
    [principal_id] int  NOT NULL,
    [diagram_id] int IDENTITY(1,1) NOT NULL,
    [version] int  NULL,
    [definition] varbinary(max)  NULL
);
GO

-- Creating table 'SACH_FULL'
CREATE TABLE [dbo].[SACH_FULL] (
    [MaSach] char(10)  NOT NULL,
    [TenSach] nvarchar(60)  NULL,
    [TenNXB] nvarchar(50)  NULL,
    [TenNhom] nvarchar(10)  NULL,
    [SoTrang] int  NULL,
    [GiaGoc] int  NULL,
    [GiaBan] int  NULL,
    [TrangBia] varbinary(max)  NULL
);
GO

-- Creating table 'CONFIGIDs'
CREATE TABLE [dbo].[CONFIGIDs] (
    [bookid] int  NOT NULL,
    [accountid] int  NULL,
    [commentid] int  NULL
);
GO

-- --------------------------------------------------
-- Creating all PRIMARY KEY constraints
-- --------------------------------------------------

-- Creating primary key on [MaLoai] in table 'LOAISACHes'
ALTER TABLE [dbo].[LOAISACHes]
ADD CONSTRAINT [PK_LOAISACHes]
    PRIMARY KEY CLUSTERED ([MaLoai] ASC);
GO

-- Creating primary key on [MaNXB] in table 'NHAXUATBANs'
ALTER TABLE [dbo].[NHAXUATBANs]
ADD CONSTRAINT [PK_NHAXUATBANs]
    PRIMARY KEY CLUSTERED ([MaNXB] ASC);
GO

-- Creating primary key on [MaNhom] in table 'NHOMSACHes'
ALTER TABLE [dbo].[NHOMSACHes]
ADD CONSTRAINT [PK_NHOMSACHes]
    PRIMARY KEY CLUSTERED ([MaNhom] ASC);
GO

-- Creating primary key on [MaSach] in table 'SACHes'
ALTER TABLE [dbo].[SACHes]
ADD CONSTRAINT [PK_SACHes]
    PRIMARY KEY CLUSTERED ([MaSach] ASC);
GO

-- Creating primary key on [diagram_id] in table 'sysdiagrams'
ALTER TABLE [dbo].[sysdiagrams]
ADD CONSTRAINT [PK_sysdiagrams]
    PRIMARY KEY CLUSTERED ([diagram_id] ASC);
GO

-- Creating primary key on [MaSach] in table 'SACH_FULL'
ALTER TABLE [dbo].[SACH_FULL]
ADD CONSTRAINT [PK_SACH_FULL]
    PRIMARY KEY CLUSTERED ([MaSach] ASC);
GO

-- Creating primary key on [bookid] in table 'CONFIGIDs'
ALTER TABLE [dbo].[CONFIGIDs]
ADD CONSTRAINT [PK_CONFIGIDs]
    PRIMARY KEY CLUSTERED ([bookid] ASC);
GO

-- --------------------------------------------------
-- Creating all FOREIGN KEY constraints
-- --------------------------------------------------

-- Creating foreign key on [Loai] in table 'NHOMSACHes'
ALTER TABLE [dbo].[NHOMSACHes]
ADD CONSTRAINT [FK_NHOMSACH_LOAISACH]
    FOREIGN KEY ([Loai])
    REFERENCES [dbo].[LOAISACHes]
        ([MaLoai])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_NHOMSACH_LOAISACH'
CREATE INDEX [IX_FK_NHOMSACH_LOAISACH]
ON [dbo].[NHOMSACHes]
    ([Loai]);
GO

-- Creating foreign key on [NhaXuatBan] in table 'SACHes'
ALTER TABLE [dbo].[SACHes]
ADD CONSTRAINT [FK_SACH_NHAXUATBAN]
    FOREIGN KEY ([NhaXuatBan])
    REFERENCES [dbo].[NHAXUATBANs]
        ([MaNXB])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_SACH_NHAXUATBAN'
CREATE INDEX [IX_FK_SACH_NHAXUATBAN]
ON [dbo].[SACHes]
    ([NhaXuatBan]);
GO

-- Creating foreign key on [NhomSach] in table 'SACHes'
ALTER TABLE [dbo].[SACHes]
ADD CONSTRAINT [FK_SACH_NHOMSACH]
    FOREIGN KEY ([NhomSach])
    REFERENCES [dbo].[NHOMSACHes]
        ([MaNhom])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_SACH_NHOMSACH'
CREATE INDEX [IX_FK_SACH_NHOMSACH]
ON [dbo].[SACHes]
    ([NhomSach]);
GO

-- --------------------------------------------------
-- Script has ended
-- --------------------------------------------------