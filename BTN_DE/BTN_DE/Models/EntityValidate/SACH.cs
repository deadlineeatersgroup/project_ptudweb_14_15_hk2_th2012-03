﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;


namespace BTN_DE.Models
{
    [MetadataType(typeof(SACHMetaData))] 
    public partial class SACH
    {
    }
    public partial class SACHMetaData
    {
        [Required(ErrorMessage = "Tên sách không được để trống!")]
        [Display(Name = "Tên sách: ")]
        [StringLength(200, ErrorMessage = "Tên sách không được ít hon {2} kí tự và không vượt quá {1} kí tự", MinimumLength = 4)]
        public string TenSach { get; set; }

        [Display(Name = "Tác Giả: ")]
        [Required(ErrorMessage = "Tên tác giả không được để trống!")]
        public string TacGia { get; set; }

        [Display(Name = "Số Trang: ")]
        [Required(ErrorMessage = "Vui lòng nhập đúng số trang!")]
        public Nullable<int> SoTrang { get; set; }

        [Display(Name = "Giá Bán: ")]
        [Required(ErrorMessage = "Vui lòng nhập giá bán")]
        public Nullable<int> GiaBan { get; set; }

        [Display(Name = "Nhóm Sách: ")]
        public string NhomSach { get; set; }

        [Display(Name = "Trang Bìa: ")]
        public string TrangBia { get; set; }

        [Display(Name = "Nhà Xuất Bản: ")]
        public string NhaXuatBan { get; set; }

        [Display(Name = "Thông Tin Mô Tả: ")]
        public string ThongTinMoTa { get; set; }

        [Display(Name = "Lời Giới Thiệu:  ")]
        public string GioiThieuSach { get; set; }
    }
}