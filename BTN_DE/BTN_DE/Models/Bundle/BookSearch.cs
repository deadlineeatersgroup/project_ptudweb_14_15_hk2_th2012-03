﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BTN_DE.Models.Bundle
{
    public class BookSearch
    {
        public string Name { get; set; }
        public string Provide { get; set; }
        public string Group { get; set; }
        public int Cost { get; set; }
    }
}