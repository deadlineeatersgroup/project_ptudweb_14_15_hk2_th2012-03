﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Reflection;
using System.Web.Mvc;

namespace BTN_DE.Models
{
    public class ExternalLoginConfirmationViewModel
    {
        [Required]
        [Display(Name = "Email")]
        public string Email { get; set; }
    }

    public class ExternalLoginListViewModel
    {
        public string ReturnUrl { get; set; }
    }

    public class SendCodeViewModel
    {
        public string SelectedProvider { get; set; }
        public ICollection<System.Web.Mvc.SelectListItem> Providers { get; set; }
        public string ReturnUrl { get; set; }
    }

    public class VerifyCodeViewModel
    {
        [Required]
        public string Provider { get; set; }

        [Required]
        [Display(Name = "Code")]
        public string Code { get; set; }
        public string ReturnUrl { get; set; }

        [Display(Name = "Remember this browser?")]
        public bool RememberBrowser { get; set; }
    }

    public class ForgotViewModel
    {
        [Required]
        [Display(Name = "Email")]
        public string Email { get; set; }
    }

    public class LoginViewModel
    {
        [Required(ErrorMessage = "'Tên đăng nhập' chưa được nhập.")]
        [Display(Name = "Tên đăng nhập")]
        public string Username { get; set; }

        [Required(ErrorMessage = "'Mật khẩu' chưa được nhập.")]
        [DataType(DataType.Password)]
        [Display(Name = "Mật khẩu")]
        public string Password { get; set; }

        [Display(Name = "Tự động đăng nhập?")]
        public bool RememberMe { get; set; }

        [DataType(DataType.EmailAddress, ErrorMessage = "Định dạng 'Email' không hợp lệ.")]
        [Display(Name = "Email")]
        public string Email { get; set; }
    }

    public class RegisterViewModel
    {
        [Required(ErrorMessage = "'Tên đăng nhập' chưa được nhập.")]
        [MinLength(8, ErrorMessage = "'Tên đăng nhập' phải có ít nhất 8 ký tự.")]
        [MaxLength(30, ErrorMessage = "'Tên đăng nhập' phải có tối đa 30 ký tự.")]
        [Display(Name = "Tên đăng nhập")]
        public string Username { get; set; }

        [MaxLength(1, ErrorMessage = "'Tên đăng nhập' này đã được sử dụng.")]
        public string UniqueUsernameError { get; set; }

        [Required(ErrorMessage = "'Mật khẩu' chưa được nhập.")]
        [MinLength(6, ErrorMessage = "'Mật khẩu' phải có ít nhất 6 ký tự.")]
        [MaxLength(50, ErrorMessage = "'Mật khẩu' phải có tối đa 50 ký tự.")]
        [DataType(DataType.Password)]
        [Display(Name = "Mật khẩu")]
        public string Password { get; set; }

        [Required(ErrorMessage = "'Nhập lại mật khẩu' chưa được nhập.")]
        [DataType(DataType.Password)]
        [Display(Name = "Nhập lại mật khẩu")]
        [System.ComponentModel.DataAnnotations.Compare("Password",
            ErrorMessage = "'Nhập lại mật khẩu' không khớp với 'Mật khẩu'.")]
        public string ConfirmPassword { get; set; }

        [Required(ErrorMessage = "'Email' chưa được nhập.")]
        [DataType(DataType.EmailAddress,
            ErrorMessage = "Định dạng 'Email' không hợp lệ.")]
        [Display(Name = "Email")]
        public string Email { get; set; }

        [MaxLength(1, ErrorMessage = "'Email' này đã được sử dụng.")]
        public string UniqueEmailError { get; set; }

        [Required(ErrorMessage = "'Nhập lại Email' chưa được nhập.")]
        [DataType(DataType.EmailAddress)]
        [Display(Name = "Nhập lại Email")]
        [System.ComponentModel.DataAnnotations.Compare("Email", ErrorMessage = "'Nhập lại Email' không khớp với 'Email'.")]
        public string ConfirmEmail { get; set; }

        //[Required(ErrorMessage = "{0} chưa được nhập.")]
        [MaxLength(100, ErrorMessage = "'Tên của bạn' phải có tối đa 100 ký tự.")]
        [Display(Name = "Tên của bạn")]
        public string Name { get; set; }

        [Display(Name = "Giới tính")]
        public int Gender { get; set; }
        public IEnumerable<SelectListItem> GenderList { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd/mm/yyyy}", ApplyFormatInEditMode = true)]
        [Display(Name = "Ngày tháng năm sinh")]
        public DateTime? Birthday { get; set; }

        [MaxLength(200, ErrorMessage = "'Nơi ở hiện tại' phải có tối đa 200 ký tự.")]
        [Display(Name = "Nơi ở hiện tại")]
        public string Address { get; set; }

        [MinLength(3, ErrorMessage = "'Số điện thoại' phải có ít nhất 3 ký tự.")]
        [MaxLength(20, ErrorMessage = "'Số điện thoại' có 'số chữ số' vượt quá mức cho phép")]
        [DataType(DataType.PhoneNumber)]
        [Display(Name = "Số điện thoại")]
        public string PhoneNumber { get; set; }

        public RegisterViewModel()
        {
            GenderList = new List<SelectListItem>();
            IEnumerable<Gender> genderType = Enum.GetValues(typeof(Gender)).Cast<Gender>();
            GenderList = from gender in genderType
                         select new SelectListItem
                         {
                             Text = GetDescription(gender),
                             Value = ((int)gender).ToString()
                         };
        }

        public string GetDescription(Enum value)
        {
            Type type = value.GetType();
            string name = Enum.GetName(type, value);

            if (name != null)
            {
                FieldInfo field = type.GetField(name);
                if (field != null)
                {
                    DescriptionAttribute attr = Attribute.GetCustomAttribute(field,
                        typeof(DescriptionAttribute)) as DescriptionAttribute;
                    if (attr != null)
                    {
                        return attr.Description;
                    }
                }
            }

            return null;
        }
    }

    public class ResetPasswordViewModel
    {
        [Required(ErrorMessage = "'Email' chưa được nhập.")]
        [DataType(DataType.EmailAddress,
            ErrorMessage = "Định dạng 'Email' không hợp lệ.")]
        [Display(Name = "Email")]
        public string Email { get; set; }

        [Required(ErrorMessage = "'Mật khẩu' chưa được nhập.")]
        [MinLength(6, ErrorMessage = "'Mật khẩu' phải có ít nhất 6 ký tự.")]
        [MaxLength(50, ErrorMessage = "'Mật khẩu' phải có tối đa 50 ký tự.")]
        [DataType(DataType.Password)]
        [Display(Name = "Mật khẩu")]
        public string Password { get; set; }

        [Required(ErrorMessage = "'Nhập lại mật khẩu' chưa được nhập.")]
        [DataType(DataType.Password)]
        [Display(Name = "Nhập lại mật khẩu")]
        [System.ComponentModel.DataAnnotations.Compare("Password",
            ErrorMessage = "'Nhập lại mật khẩu' không khớp với 'Mật khẩu'.")]
        public string ConfirmPassword { get; set; }

        public string Code { get; set; }
    }

    public class ForgotPasswordViewModel
    {
        [Required(ErrorMessage = "'Email' chưa được nhập.")]
        [DataType(DataType.EmailAddress,
            ErrorMessage = "Định dạng 'Email' không hợp lệ.")]
        [Display(Name = "Email")]
        public string Email { get; set; }
    }
}