//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace BTN_DE.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class NHOMSACH
    {
        public NHOMSACH()
        {
            this.SACHes = new HashSet<SACH>();
        }
    
        public string MaNhom { get; set; }
        public string TenNhom { get; set; }
        public string Loai { get; set; }
    
        public virtual LOAISACH LOAISACH { get; set; }
        public virtual ICollection<SACH> SACHes { get; set; }
    }
}
