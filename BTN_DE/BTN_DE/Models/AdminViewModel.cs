﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using Microsoft.AspNet.Identity.EntityFramework;
using BTN_DE.Controllers.Admin;
using BTN_DE.Lib;
using System;
using System.Linq;
using System.Reflection;
using System.ComponentModel;

namespace BTN_DE.Models
{
    public class RoleIndexViewModel
    {
        public PagingInformation<IdentityRole> PagingInformation { get; set; } 
    }

    public class RoleViewModel
    {
        public string Id { get; set; }

        [Required(AllowEmptyStrings = false)]
        [Display(Name = "Vai trò")]
        public string Name { get; set; }

        [Display(Name = "Thông báo lỗi")]
        public string ErrorMessage { get; set; }
    }

    public class EditRoleViewModel
    {
        public IdentityRole Role { get; set; }

        [Display(Name = "Thông báo lỗi")]
        public string ErrorMessage { get; set; }
    }

    public class UserIndexViewModel
    {
        public PagingInformation<ApplicationUser> PagingInformation { get; set; } 
    }

    public class UserDetailsViewModel
    {
        [Display(Name = "Định danh")]
        public string Id { get; set; }

        [Display(Name = "Tên đăng nhập")]
        public string Username { get; set; }

        [DataType(DataType.EmailAddress,
            ErrorMessage = "Định dạng 'Email' không hợp lệ.")]
        [Display(Name = "Email")]
        public string Email { get; set; }

        [Display(Name = "Email đã được xác thực")]
        public bool EmailConfirmed { get; set; }

        [Display(Name = "Ngày đăng ký")]
        public DateTime? RegisterDate { get; set; }

        [Display(Name = "Tên của bạn")]
        public string Name { get; set; }

        [Display(Name = "Giới tính")]
        public bool Gender { get; set; }

        [Display(Name = "Ngày tháng năm sinh")]
        public DateTime? Birthday { get; set; }

        [Display(Name = "Nơi ở hiện tại")]
        public string Address { get; set; }

        [DataType(DataType.PhoneNumber)]
        [Display(Name = "Số điện thoại")]
        public string PhoneNumber { get; set; }
    }

    public class EditUserViewModel
    {
        public string Id { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "'Email' chưa được nhập.")]
        [DataType(DataType.EmailAddress,
            ErrorMessage = "Định dạng 'Email' không hợp lệ.")]
        [Display(Name = "Email")]
        public string Email { get; set; }

        public IEnumerable<SelectListItem> RolesList { get; set; }

        [MaxLength(100, ErrorMessage = "'Tên của bạn' phải có tối đa 100 ký tự.")]
        [Display(Name = "Tên của bạn")]
        public string Name { get; set; }

        [Display(Name = "Giới tính")]
        public int Gender { get; set; }
        public IEnumerable<SelectListItem> GenderList { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd/mm/yyyy}", ApplyFormatInEditMode = true)]
        [Display(Name = "Ngày tháng năm sinh")]
        public DateTime? Birthday { get; set; }

        [MaxLength(200, ErrorMessage = "'Nơi ở hiện tại' phải có tối đa 200 ký tự.")]
        [Display(Name = "Nơi ở hiện tại")]
        public string Address { get; set; }

        [MinLength(3, ErrorMessage = "'Số điện thoại' phải có ít nhất 3 ký tự.")]
        [MaxLength(20, ErrorMessage = "'Số điện thoại' có 'số chữ số' vượt quá mức cho phép")]
        [DataType(DataType.PhoneNumber)]
        [Display(Name = "Số điện thoại")]
        public string PhoneNumber { get; set; }

        public EditUserViewModel()
        {
            GenderList = new List<SelectListItem>();
            IEnumerable<Gender> genderType = Enum.GetValues(typeof(Gender)).Cast<Gender>();
            GenderList = from gender in genderType
                         select new SelectListItem
                         {
                             Text = GetDescription(gender),
                             Value = ((int)gender).ToString()
                         };
        }

        public string GetDescription(Enum value)
        {
            Type type = value.GetType();
            string name = Enum.GetName(type, value);

            if (name != null)
            {
                FieldInfo field = type.GetField(name);
                if (field != null)
                {
                    DescriptionAttribute attr = Attribute.GetCustomAttribute(field,
                        typeof(DescriptionAttribute)) as DescriptionAttribute;
                    if (attr != null)
                    {
                        return attr.Description;
                    }
                }
            }

            return null;
        }
    }

    public class DeleteUserViewModel
    {
        [Display(Name = "Định danh")]
        public string Id { get; set; }

        [Display(Name = "Tên đăng nhập")]
        public string Username { get; set; }

        [Display(Name = "Email")]
        public string Email { get; set; }

        [Display(Name = "Thông báo lỗi")]
        public string ErrorMessage { get; set; }
    }
}