﻿//Load the students in the List and displays when the documnet is loaded
$(document).ready(function () {
    jQuery.support.cors = true;
    $.ajax({
        url: '/api/Product',
        type: 'GET',
        dataType: 'json',
        success: function (data) {
            WriteResponses(data);
        },
        error: function (x, y, z) {
            alert(x + '\n' + y + '\n' + z);
        }
    });

    //Displays in a Table
    function WriteResponses(students) {
        var strResult = "<table><th>Name</th><th>Student ID</th><th>Gender</th><th>Age</th>";
        $.each(students, function (index, student) {
            strResult += "<tr><td>" + student.name + "</td><td> " + student.id + "</td><td>" + student.gender + "</td><td>" + student.age + "</td></tr>";
        });
        strResult += "</table>";
        $("#divResult").html(strResult);
    }

});
